# My Spring-Boot Notes
## Project Creation
* Spring Initializer ---> https://start.spring.io/
## Maven Central
https://mvnrepository.com/
## spring-boot-starter JAR
* spring-boot-starter
* spring-boot-starter-test
* spring-boot-starter-web
* spring-boot-starter-actuator
* spring-boot-starter-aop
* spring-boot-starter-security
* spring-boot-starter-validation
* spring-boot-starter-log4j2
* spring-boot-starter-jdbc
* spring-boot-starter-mail
* spring-boot-starter-logging
* spring-boot-starter-websocket
* spring-boot-starter-tomcat
* spring-boot-starter-webflux
* spring-boot-starter-thymeleaf
* spring-boot-starter-undertow
* spring-boot-starter-jetty
* spring-boot-starter-cache
* spring-boot-starter-amqp
* spring-boot-starter-freemarker
* spring-boot-starter-jersey
* spring-boot-starter-hateoas
* spring-boot-starter-json
* spring-boot-starter-integration
* spring-boot-starter-quartz
* spring-boot-starter-activemq
### spring-boot-starter-data (https://docs.spring.io/spring-data/)
* spring-boot-starter-data-redis ---> https://docs.spring.io/spring-data/redis/docs/current/reference/html/#preface
* spring-boot-starter-data-jpa ---> https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#preface
* spring-boot-starter-data-mongodb ---> https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#preface
* spring-boot-starter-data-rest ---> https://docs.spring.io/spring-data/rest/docs/current/reference/html/#preface
* spring-boot-starter-data-elasticsearch
* spring-boot-starter-data-jdbc ---> https://docs.spring.io/spring-data/jdbc/docs/current/reference/html/#preface
* spring-boot-starter-data-cassandra ---> https://docs.spring.io/spring-data/cassandra/docs/current/reference/html/#preface
* spring-boot-starter-data-r2dbc
* spring-boot-starter-data-neo4j
* spring-boot-starter-data-couchbase ---> https://docs.spring.io/spring-data/couchbase/docs/current/reference/html/#preface
* spring-boot-starter-data-solr
* spring-boot-starter-data-ldap
* spring-boot-starter-data-jest
## resources/application.properties OR Common Application properties
source:
https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html

source for SERVER configs: 
https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#common-application-properties-server

**NOTE** The Java class is inside Spring Boot project:

    org/springframework/boot/autoconfigure/web/ServerProperties.java

## Application Profile
### Diagram
![ApplicationProfile](images/ApplicationProfile.png)
### How to activat a profile?

    spring.profiles.active

source: https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#spring.profiles.active

# ==============================
# JPA
## Diagrams
![JpaRepositoryClassHierarchy](images/JpaRepositoryClassHierarchy.png)

![QueryDSL](images/QueryDSL.png)

![ORMwithJPA](images/ORMwithJPA.png)
## Tutorials
* Accessing Data with JPA ---> https://spring.io/guides/gs/accessing-data-jpa/

# JPA Relationship Mapping
## Overview
* One-to-Many
* Many-to-One
* One-to-One
  * https://www.baeldung.com/jpa-one-to-one
* Many-to-Many
## Tutorial
* Guide to JPA with Hibernate - Basic Mapping ---> https://stackabuse.com/guide-to-jpa-with-hibernate-basic-mapping/
* Guide to JPA with Hibernate - Relationship Mapping ---> https://stackabuse.com/a-guide-to-jpa-with-hibernate-relationship-mapping/
* Map Associations with JPA and Hibernate – The Ultimate Guide ---> https://thorben-janssen.com/ultimate-guide-association-mappings-jpa-hibernate/

## Important Java Interfaces
* Repository ---> https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/Repository.html
* CrudRepository ---> https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html
* PagingAndSortingRepository ---> https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/PagingAndSortingRepository.html

## Table 3. Supported keywords inside method names

![JpaJpql01](images/JpaJpql01.png)

![JpaJpql02](images/JpaJpql02.png)

source: https://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/#jpa.query-methods.query-creation

# ==============================
# QueryDSL
## Tutorials
* Intro to Querydsl ---> https://www.baeldung.com/intro-to-querydsl
* A Guide to Querydsl with JPA ---> https://www.baeldung.com/querydsl-with-jpa-tutorial
* Advanced Spring Data JPA - Specifications and Querydsl ---> https://spring.io/blog/2011/04/26/advanced-spring-data-jpa-specifications-and-querydsl


# ==============================
# Spring Data REST
## Tutorial
* Working with Relationships in Spring Data REST ---> https://www.baeldung.com/spring-data-rest-relationships

# ==============================
# HTTP Object
## HTTP Header
* How to Read HTTP Headers in Spring REST Controllers ---> https://www.baeldung.com/spring-rest-http-headers
* How to Set a Header on a Response with Spring 5 ---> https://www.baeldung.com/spring-response-header
* ResponseEntity vs HttpServletRequest ---> https://stackoverflow.com/questions/28209242/reading-http-headers-in-a-spring-rest-controller
## HTTP Body
* Get HTTP POST Body in Spring Boot with @RequestBody ---> https://stackabuse.com/get-http-post-body-in-spring/
* Spring’s RequestBody and ResponseBody Annotations ---> https://www.baeldung.com/spring-request-response-body
* Using Spring ResponseEntity to Manipulate the HTTP Response ---> https://www.baeldung.com/spring-response-entity
## Java Class
### Diagram
![ResponseEntity](images/ResponseEntity.png)

![ClassDiagramOfHttpServlet](images/ClassDiagramOfHttpServlet.png)

* HttpServlet ---> https://docs.oracle.com/javaee/7/api/javax/servlet/http/HttpServlet.html
* ResponseEntity ---> https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/http/ResponseEntity.html
### Tutorials
* Using Spring ResponseEntity to Manipulate the HTTP Response ---> https://www.baeldung.com/spring-response-entity
* ResponseEntity vs ResponseBody ---> https://stackoverflow.com/questions/26549379/when-use-responseentityt-and-restcontroller-for-spring-restful-applications

# ==============================
# File Manipulation
* Uploading Files Tutorial ---> https://spring.io/guides/gs/uploading-files/
* Uploading Files with Spring Boot ---> https://stackabuse.com/uploading-files-with-spring-boot/

## Cross-Origin Resource Sharing (CORS)
* Enabling Cross Origin Requests for a RESTful Web Service ---> https://spring.io/guides/gs/rest-service-cors/ 

# ==============================
# Annotations
## List
| Category | Names |
|----------|-------|
Core Spring Framework Annotations | @Required, @Autowired, @Qualifier, @Configuration, @ComponentScan, @Bean, @Lazy, @Value
Spring Framework Stereotype Annotations | @Component, @Controller, @Service, @Repository
Spring Boot Annotations | @EnableAutoConfiguration, @SpringBootApplication
Spring MVC and REST Annotations | @Controller, @RequestMapping, @CookieValue, @CrossOrigin, @RestController
Composed @RequestMapping Variants | @GetMapping, @PostMapping, @PutMapping, @PatchMapping, @DeleteMapping, 
Others | @PathVariable, @RequestAttribute, @RequestBody, @RequestHeader, @RequestParam, @RequestPart
Response | @ResponseBody, @ResponseStatus,
Session | @SessionAttribute, @SessionAttributes
Spring Cloud Annotations | @EnableConfigServer, @EnableEurekaServer, @EnableDiscoveryClient, @EnableCircuitBreaker, @HystrixCommand
Spring Framework DataAccess Annotations | @Transactional
Cache-Based Annotations | @Cacheable, @CachePut, @CacheEvict, @CacheConfig
Task Execution and Scheduling Annotations | @Scheduled, @Async
Spring Framework Testing Annotations | @BootstrapWith, @ContextConfiguration, @WebAppConfiguration, @Timed, @Repeat, @Commit, @RollBack, @DirtiesContext, @BeforeTransaction, @AfterTransaction

* Spring Boot Annotations ---> https://www.javatpoint.com/spring-boot-annotations
* Spring Boot Annotations ---> https://springframework.guru/spring-framework-annotations/
## @SpringBootApplication
@SpringBootApplication = @SpringBootConfiguration + @EnableAutoConfiguration + @ComponentScan

![AnnotationSpringBootApplication](images/AnnotationSpringBootApplication.png)

## @Entity
### @GeneratedValue

    If we want the primary key value to be generated automatically for us, we can add the @GeneratedValue annotation.
    This can use 4 generation types: AUTO, IDENTITY, SEQUENCE, TABLE.
    If we don't specify a value explicitly, the generation type defaults to AUTO.

source:
* An Overview of Identifiers in Hibernate/JPA ---> https://www.baeldung.com/hibernate-identifiers
* How to generate primary keys with JPA and Hibernate ---> https://thorben-janssen.com/jpa-generate-primary-keys/

## @Service
### Tutorials
* Spring Boot - Service Components ---> https://www.tutorialspoint.com/spring_boot/spring_boot_service_components.htm
* What's the difference between @Component, @Repository & @Service annotations in Spring? ---> https://stackoverflow.com/questions/6827752/whats-the-difference-between-component-repository-service-annotations-in
* @Component vs @Repository and @Service in Spring ---> https://www.baeldung.com/spring-component-repository-service
# ==============================
# Docker
![SpringBootDockerfileExample](images/SpringBootDockerfileExample.png)

# ==============================
## Spring Ecosystem
### Diagram

![SpringFrameworkEcosystem](images/SpringFrameworkEcosystem.png)

### Overview
#### Web Layer
* Spring HATEOAS
* Spring Mobile
* Spring Web Flow
* Spring Session
* Spring Web Services
* Spring Social
* Spring for Android
* Spring Security
#### Common Layer
* Spring Web
* Spring Test
* Spring Data Access/Integration
* Spring AOP and Instrumentation
* Spring Messaging
* Spring Core Container
#### Service Layer
* Spring Integration
* Spring Cloud
#### Data Layer
* Spring AMQP
* Spring LDAP
* Spring Data
* Spring Batch
#### References
* Spring Framework Ecosystem ---> http://springtutorials.com/spring-ecosystem/
* SPRING IO 2017: THE SPRING ECOSYSTEM ---> https://ordina-jworks.github.io/spring/2017/06/07/Spring-IO-2017-The-Spring-ecosystem.html

# ==============================
# Web Service
## Tutorials
* Building REST services with Spring ---> https://spring.io/guides/tutorials/rest/
* Building a RESTful Web Service with Spring Boot Actuator ---> https://spring.io/guides/gs/actuator-service/
* Building a RESTful Web Service ---> https://spring.io/guides/gs/rest-service/
