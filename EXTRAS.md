# URL vs URI vs URN
![URLResourceName](images/URLResourceName.png)

![URLvsURI](images/URLvsURI.png)

# REST API
![RESTVisually](images/RESTVisually.png)

# ==============================
# SQL Aggregate Functions
## SQL Aggregate Functions
* COUNT()
* SUM()
* MIN() MAX()
* AVG()

source: https://mode.com/sql-tutorial/sql-aggregate-functions/
## Aggregate Functions (Transact-SQL)
Transact-SQL provides the following aggregate functions:

* APPROX_COUNT_DISTINCT
* AVG
* CHECKSUM_AGG
* COUNT
* COUNT_BIG
* GROUPING
* GROUPING_ID
* MAX
* MIN
* STDEV
* STDEVP
* STRING_AGG
* SUM
* VAR
* VARP

source: https://docs.microsoft.com/en-us/sql/t-sql/functions/aggregate-functions-transact-sql?view=sql-server-ver15
## MySQL Aggregate Functions
### W3School
* MySQL String Functions
* MySQL Numeric Functions
* MySQL Date Functions
* MySQL Advanced Functions
source: https://www.w3schools.com/sql/sql_ref_mysql.asp
### Official Doc
Table 12.25 Aggregate Functions

| Name | Description |
|------|-------------|
AVG()	| Return the average value of the argument
BIT_AND()	| Return bitwise AND
BIT_OR()	| Return bitwise OR
BIT_XOR()	| Return bitwise XOR
COUNT()	| Return a count of the number of rows returned
COUNT(DISTINCT)	| Return the count of a number of different values
GROUP_CONCAT()	| Return a concatenated string
JSON_ARRAYAGG()	| Return result set as a single JSON array
JSON_OBJECTAGG()	| Return result set as a single JSON object
MAX()	| Return the maximum value
MIN()	| Return the minimum value
STD()	| Return the population standard deviation
STDDEV()	| Return the population standard deviation
STDDEV_POP()	| Return the population standard deviation
STDDEV_SAMP()	| Return the sample standard deviation
SUM()	| Return the sum
VAR_POP()	| Return the population standard variance
VAR_SAMP()	| Return the sample variance
VARIANCE()	| Return the population standard variance

source: https://dev.mysql.com/doc/refman/8.0/en/aggregate-functions.html

# ==============================
# JWT vs OAuth?
![OAuth2Stack](images/OAuth2Stack.png)
