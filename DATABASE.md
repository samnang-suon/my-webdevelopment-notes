# My Database Notes
## Database Development Life cycle
![DatabaseDevelopmentLifecycle](images/DatabaseDevelopmentLifecycle.png)

## SQL Commands Family
### Diagram
![SqlCommandFamily01](images/SqlCommandFamily01.png)

![SqlCommandFamily02](images/SqlCommandFamily02.jpg)

![SqlCommandFamily03](images/SqlCommandFamily03.png)
### Acronym
* DDL = Data Definition Language
* DML = Data Manipulation Language
* DCL = Data Control Language
* TCL = Transact Control Language
* DQL = Data Query Language
### Sources:
* https://stackoverflow.com/questions/2578194/what-are-ddl-and-dml
* https://www.geeksforgeeks.org/sql-ddl-dml-tcl-dcl/
* DDL ---> https://en.wikipedia.org/wiki/Data_definition_language
* DML ---> https://en.wikipedia.org/wiki/Data_manipulation_language
* DCL ---> https://en.wikipedia.org/wiki/Data_control_language

## Diagram Tools / Modeler Tools
* Lucidchart ---> https://www.lucidchart.com/pages/
* SqlDBM ---> https://sqldbm.com/Home/
* DB Diagram ---> https://dbdiagram.io/home
* QuickDBD ---> https://www.quickdatabasediagrams.com/

## Data Types
![DatabaseDataTypes](images/DatabaseDataTypes.png)

## EXTRAS
* Search for "database design life cycle"
* Search for "database development life cycle"